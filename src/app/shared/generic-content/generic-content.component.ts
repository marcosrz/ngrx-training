import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generic-content',
  templateUrl: './generic-content.component.html',
  styleUrls: ['./generic-content.component.scss']
})
export class GenericContentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
