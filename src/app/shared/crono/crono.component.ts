import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-crono',
  templateUrl: './crono.component.html',
  styleUrls: ['./crono.component.scss']
})
export class CronoComponent implements OnInit {

  @Output() onStart = new EventEmitter();
  @Output() onStop = new EventEmitter();

  interval: any;
  counter = 0;

  constructor() { }

  ngOnInit(): void {
  }

  _increase(){
    this.counter++;
  }

  play(){
    if (!this.interval){
      this.interval = setInterval(() => {
        this._increase()
      }, 10)
      this.onStart.emit(this.counter);
    }
  }

  stop(){
    if (this.interval){
      clearInterval(this.interval);
      this.interval = null;
      this.onStop.emit(this.counter);
    }
  }

}
