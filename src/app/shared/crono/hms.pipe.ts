import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hms'
})
export class HmsPipe implements PipeTransform {

  private _padTo2(n: number) {
    return String(n).padStart(2, '0');
  };

  transform(value: number, ...args: unknown[]): unknown {

    let rest = value;

    const hours = Math.floor(rest / 3600);
    rest = Math.floor(rest % 3600);

    const minutes = Math.floor((rest / 60));
    rest = Math.floor(rest % 60)

    const seconds = Math.floor(rest);

    return `${this._padTo2(hours)}:${this._padTo2(minutes)}:${this._padTo2(seconds)}`;
  }

}
