import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldsetItemComponent } from './fieldset-item.component';

describe('FieldsetItemComponent', () => {
  let component: FieldsetItemComponent;
  let fixture: ComponentFixture<FieldsetItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FieldsetItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldsetItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
