import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExercisesTableComponent } from './exercises-table/exercises-table.component';
import { TrainingIntensityComponent } from './training-intensity/training-intensity.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldsetComponent } from './fieldset/fieldset.component';
import { FormComponent } from './form/form.component';
import { FieldsetItemComponent } from './fieldset-item/fieldset-item.component';
import { CreateExerciseInlineComponent } from './create-exercise-inline/create-exercise-inline.component';
import { CronoComponent } from './crono/crono.component';
import { HmsPipe } from './crono/hms.pipe';
import { IconsModule } from './icons/icons.module';
import { GenericHeaderComponent } from './generic-header/generic-header.component';
import { GenericContentComponent } from './generic-content/generic-content.component';
import { FeatherComponent } from 'angular-feather';
import { CollapsibleComponent } from './collapsible/collapsible.component';



@NgModule({
  declarations: [
    ExercisesTableComponent,
    TrainingIntensityComponent,
    FieldsetComponent,
    FieldsetItemComponent,
    FormComponent,
    FieldsetItemComponent,
    CreateExerciseInlineComponent,
    CronoComponent,
    HmsPipe,
    GenericHeaderComponent,
    GenericContentComponent,
    CollapsibleComponent,
  ],
  exports: [
    ExercisesTableComponent,
    TrainingIntensityComponent,
    FieldsetComponent,
    FieldsetItemComponent,
    FormComponent,
    FieldsetItemComponent,
    CreateExerciseInlineComponent,
    CronoComponent,
    HmsPipe,
    GenericHeaderComponent,
    GenericContentComponent,
    FeatherComponent,
    CollapsibleComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    IconsModule
  ]
})
export class SharedModule { }
