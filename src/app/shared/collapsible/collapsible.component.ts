import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-collapsible',
  templateUrl: './collapsible.component.html',
  styleUrls: ['./collapsible.component.scss']
})
export class CollapsibleComponent implements OnInit {

  toggle = false;

  constructor() { }

  ngOnInit(): void {
  }

  onToggle(){
    this.toggle = !this.toggle;
  }

}
