import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Exercise } from 'src/app/model/exercise.model';
import { Training } from 'src/app/model/training.model';

@Component({
  selector: 'app-training-intensity',
  templateUrl: './training-intensity.component.html',
  styleUrls: ['./training-intensity.component.scss']
})
export class TrainingIntensityComponent implements OnInit {

  @Input() exercises?: Exercise[];

  constructor() {}
  
  ngOnInit(): void {}
  
  calculateIntensity(){

    return this.exercises ?
      this.exercises.map(t => t.reps * t.series * t.strength).reduce((p,q) => p + q, 0)
      : 0;
  }

}
