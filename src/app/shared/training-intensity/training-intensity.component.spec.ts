import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingIntensityComponent } from './training-intensity.component';

describe('TrainingIntensityComponent', () => {
  let component: TrainingIntensityComponent;
  let fixture: ComponentFixture<TrainingIntensityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingIntensityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingIntensityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
