import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateExerciseInlineComponent } from './create-exercise-inline.component';

describe('CreateExerciseInlineComponent', () => {
  let component: CreateExerciseInlineComponent;
  let fixture: ComponentFixture<CreateExerciseInlineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateExerciseInlineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateExerciseInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
