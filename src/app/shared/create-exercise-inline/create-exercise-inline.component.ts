import { Component, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Exercise } from 'src/app/model/exercise.model';
import { EventEmitter } from '@angular/core'

@Component({
  selector: 'app-create-exercise-inline',
  templateUrl: './create-exercise-inline.component.html',
  styleUrls: ['./create-exercise-inline.component.scss']
})
export class CreateExerciseInlineComponent implements OnInit {
  exercise?: Exercise;
  form?: FormGroup;
  editMode = false;

  @Output() OnNewExercise = new EventEmitter();

  constructor(
  ) {}

  ngOnInit(): void {
    this.initForm();
  }
  
  private initForm(){
    
    let name = '';
    let description = '';
    let series = null;
    let reps = null;
    let strength = null;
    let unit = null;

    if (this.exercise !== undefined){
      name = this.exercise.name;
      description = this.exercise.description;
      series = this.exercise.series;
      reps = this.exercise.reps;
      strength = this.exercise.strength;
      unit = this.exercise.unit;
    }

    this.form = new FormGroup({
      'name': new FormControl(name),
      'description': new FormControl(description),
      'series': new FormControl(series),
      'reps': new FormControl(reps),
      'strength': new FormControl(strength),
      'unit': new FormControl(unit),
    });

  }

  onAddTraining(){
    if (this.form){
      console.log(this.form?.value)
      this.OnNewExercise.emit(this.form?.value);
    }
  }
}
