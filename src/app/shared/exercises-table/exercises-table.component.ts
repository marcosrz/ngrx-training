import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Exercise } from 'src/app/model/exercise.model';

@Component({
  selector: 'app-exercises-table',
  templateUrl: './exercises-table.component.html',
  styleUrls: ['./exercises-table.component.scss']
})
export class ExercisesTableComponent implements OnInit {

  @Input() exercises?: Exercise[] = [];
  @Input() actions?: boolean = false;
  @Input() crono?: boolean = false;

  @Output() onRemoveExercise = new EventEmitter();

  constructor() { 
  }

  ngOnInit(): void {
  }

  onRemove(index: number){
    console.log('Clicked', index)
    this.onRemoveExercise.emit(index);
  }

}
