import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';

const routes: Routes = [
  {
    path: 'session',
    component: MainLayoutComponent,
    loadChildren: () => import('./feature/session/session.module').then(m => m.SessionModule)
  },
  {
    path: 'training',
    component: MainLayoutComponent,
    loadChildren: () => import('./feature/training/training.module').then(m => m.TrainingModule)
  },

  {
    path: '',
    redirectTo: 'training',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
