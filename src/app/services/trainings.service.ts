import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Training } from '../model/training.model';

const ROOT = 'https://trainings-d4187-default-rtdb.europe-west1.firebasedatabase.app/'
@Injectable({
  providedIn: 'root'
})
export class TrainingsService {

  constructor (private http: HttpClient) {}

  getTrainings():Observable<Training[]>{
    return this.http.get(`${ROOT}/trainings.json`)
      .pipe(
        map(dict => Object.entries(dict).map(x => ({ id: x[0], ...x[1] })))
      )
  }
  
  getTraining(id: string): Observable<Training>{
    return this.http.get<Training>(`${ROOT}/trainings/${id}.json`)
      .pipe(
        map(item => ({ ...item, exercises: item.exercises || [], id}))
      )
  }

  updateTraining(training: Training){
    return this.http.put<Training>(`${ROOT}/trainings/${training.id}.json`, training)
      .pipe(
        map(x => ({ ...x, id: training.id}))
      )
  }

  createTraining(training: Training): Observable<Training>{
    return this.http.post<Training>(`${ROOT}/trainings.json`, training)
      .pipe(
        map(({name}) => ({...training, id: name}))
      )
  }

  deleteTraining(id: string){
    return this.http.delete<string>(`${ROOT}/trainings/${id}.json`)
      .pipe(
        map(() => (id))
      )
  }
}
