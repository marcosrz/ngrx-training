import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Session } from '../model/session.model';
import { TrainingsService } from './trainings.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private trainingService: TrainingsService) { }

  buildSessionFromTrainingId(id: string): Observable<Session> {
    return this.trainingService.getTraining(id).pipe(
      map(training => ({...training, trainingId: training.id, id: undefined, date: new Date()}))
    )
  }
}