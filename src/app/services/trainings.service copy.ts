import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Exercise } from '../model/exercise.model';
import { Training } from '../model/training.model';


let trainings: Training[] = [];

@Injectable({
  providedIn: 'root'
})
export class TrainingsService {


  constructor () {

    trainings.push(
      {
        id: '9302jhfhj20390sd238hd823h9js',
        name: 'Entrenamiento Lunes',
        description: 'Lorem Ipsum Sit Amet',
        date: new Date(),
        exercises: [],
        tags: []
      });

      trainings.push(
      {
        id: 'DAFdqwd390sd238hd823h9js',
        name: 'Entrenamiento Martes',
        description: 'Lorem Ipsum Sit Amet',
        date: new Date(),
        exercises: [],
        tags: []
      })

      
      trainings[0].exercises.push({
        id: '89jd293j901290js1209js9102js',
        name: 'Triceps polea',
        description: 'Triceps polea description',
        tags: [],
        series: 4,
        reps: 10,
        strength: 25,
        unit: 'kg'
      })

      trainings[0].exercises.push({  
        id: 'sadqwd12d1d290js1209js9102js',
        name: 'Press banca',
        description: 'Press banca description',
        tags: [],
        series: 4,
        reps: 10,
        strength: 40,
        unit: 'kg'
      })

      trainings[1].exercises.push({
        id: '89jd293j9021es1209js9102js',
        name: 'Prensa',
        description: 'Prensa',
        tags: [],
        series: 4,
        reps: 10,
        strength: 40,
        unit: 'kg'
      });

      trainings[1].exercises.push({  
        id: '12s12ssd1d290js1209js9102js',
        name: 'Remo polea',
        description: 'Remo polea description',
        tags: [],
        series: 4,
        reps: 10,
        strength: 50,
        unit: 'kg'
      });
    

  }

  getTrainings(){
    return of([...trainings]);
  }

  getTraining(id: string): Observable<Training | undefined>{

    return of(trainings.find(t => t.id === id))

  }

  updateTraining(training: Training){


    if (!training.id) throw Error();

    const index = this._indexOf(training.id);

    const updated = {
      ...trainings[index],
      ...training
    }


    trainings[index] = updated;

    return of(updated);

  }

  createTraining(training: Training){

    trainings.push({...training, id: `TRAINING${new Date().getTime()}`})

    return of(training);

  }

  deleteTraining(id: string){

    trainings = trainings.filter(x => x.id !== id)

    return of(id);

  }

  addExerciseToTraining(id: string, exercise: Exercise){

    const index = this._indexOf(id);

    // Object.assign(dbTraining.exercises, [...dbTraining.exercises, exercise]);
    console.log(trainings[index].exercises)

    trainings[index].exercises.push(exercise)

    return of({ id, exercise })

  }

  _indexOf(id: string) {
    return trainings.findIndex(t => t.id === id);
  }
}
