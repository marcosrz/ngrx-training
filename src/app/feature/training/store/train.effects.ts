import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError, tap } from 'rxjs/operators';
import { TrainingsService } from '../../../services/trainings.service';
import { 
  addExercise, 
  createTraining, 
  deleteTraining, 
  deleteTrainingSuccess, 
  loadTrainingDetails, 
  loadTrainingDetailsFailure, 
  loadTrainingDetailsSuccess, 
  loadTrainings, 
  loadTrainingsFailure, 
  loadTrainingsSuccess, 
  updateTraining
} from './train.actions';
 
@Injectable()
export class TrainingEffects {
 
  // Get list from api
  loadTrainings$ = createEffect(() => this.actions$.pipe(
    ofType(loadTrainings),
    mergeMap(() => this.trainingsService.getTrainings()
      .pipe(
        map(trainings => loadTrainingsSuccess({trainings})),
        catchError(() => of(loadTrainingsFailure({error: 'GENERIC ERROR'})))
      ))
    )
  );

  // Get training details from API
  loadTrainingDetails$ = createEffect(() => this.actions$.pipe(
    ofType(loadTrainingDetails),
    mergeMap(({id}) => this.trainingsService.getTraining(id)
      .pipe(
        map(training => loadTrainingDetailsSuccess({training})),
        catchError(() => of(loadTrainingDetailsFailure({error: 'GENERIC ERROR'})))
      ))
  ));

  // Get training details from API
  // addExerciseToTraining$ = createEffect(() => this.actions$.pipe(
  //   tap(() => console.log('Effect triggered')),
  //   ofType(addExercise),
  //   mergeMap(({id, exercise}) => this.trainingsService.addExerciseToTraining(id, exercise)
  //     .pipe(
  //       map(({id}) => loadTrainingDetails({id})),
  //     )
  //   )
  // ));

  // Update a training
  updateTraining$ = createEffect(() => this.actions$.pipe(
    ofType(updateTraining),
    mergeMap(({ training }) => this.trainingsService.updateTraining(training)
      .pipe(
        map((training) => loadTrainingDetails({id: training.id})),
      )
    )
  ));

  // Create a training
  createTraining$ = createEffect(() => this.actions$.pipe(
    ofType(createTraining),
    mergeMap(({ training }) => this.trainingsService.createTraining(training)
      .pipe(
        map((training) => loadTrainingDetails({id: training.id})),
      )
    )
  ));

  // Remove a training
  deleteTraining$ = createEffect(() => this.actions$.pipe(
    ofType(deleteTraining),
    mergeMap(({ id }) => this.trainingsService.deleteTraining(id)
      .pipe(
        map((id) => deleteTrainingSuccess({id})),
      )
    )
  ));

  // Reload training details from API
  // reloadTrainingsList$ = createEffect(() => this.actions$.pipe(
  //   ofType(loadTrainingDetailsSuccess),
  //   map(() => loadTrainings()),
  // ));
 
  constructor(
    private actions$: Actions,
    private trainingsService: TrainingsService
  ) {}
}