import { createAction, props } from '@ngrx/store';
import { Exercise } from '../../../model/exercise.model';
import { Training } from '../../../model/training.model';

export interface LoadTrainingsSuccessPayload {
  trainings: Training[]
}

export interface LoadTrainingsFailurePayload {
  error: string
}

// Exercise Management
export const createExercise = createAction('[Exercise Component] Create Exercose');
export const deleteExercise = createAction('[Exercise Component] Delete Exercise');

// Training Management
export const deleteTraining = createAction('[Training Component] Delete Training', props<{id: string}>());
export const deleteTrainingSuccess = createAction('[Training Component] Delete Training Success', props<{id: string}>());
export const createTraining = createAction('[Training Component] Create Training', props<{training: Training}>());
export const createTrainingSuccess = createAction('[Training Component] Create Training Success', props<{training: Training}>());
export const updateTraining = createAction('[Training Component] Update Training', props<{training: Training}>());
// export const updateTrainingSuccess = createAction('[Training Component] Update Training Success', props<{training: Training}>());
// export const updateTrainingFailure = createAction('[Training Component] Update Training Failure', props<{error: string}>());
export const loadTrainingDetails = createAction('[Training Component] Load Training Details', props<{id: string}>());
export const loadTrainingDetailsSuccess = createAction('[Training Component] Load Training Details Success', props<{training: Training | undefined}>());
export const loadTrainingDetailsFailure = createAction('[Training Component] Load Training Details Failure', props<{error: string}>());

// Training exercises management
export const addExercise = createAction('[Training Component] Add Exercise', props<{id: string, exercise: Exercise}>());
export const addExerciseSuccess = createAction('[Training Component] Add Exercise Success', props<{id: string, exercise: Exercise}>());
export const addExerciseFailure = createAction('[Training Component] Add Exercise Failure', props<{error: string}>());
export const removeExercise = createAction('[Training Component] Remove Exercise');

// Training list Management
export const loadTrainings = createAction('[TrainingList Component] Load Trainings');
export const loadTrainingsSuccess = createAction('[TrainingList Component] Load Trainings Success', props<LoadTrainingsSuccessPayload>());
export const loadTrainingsFailure = createAction('[TrainingList Component] Load Trainings Failure', props<LoadTrainingsFailurePayload>());


