import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on} from '@ngrx/store';
import { Training } from '../../../model/training.model';
import * as TrainActions from './train.actions';

export const featureKey = 'training'

export interface State extends EntityState<Training> {
  loading: boolean;
  error: string | null;
  trainingDetails: Training;
}

export const adapter: EntityAdapter<Training> = createEntityAdapter<Training>();

export const initialState = adapter.getInitialState({
  loading: false,
});
 
export const reducer = createReducer(
  initialState,
  on (TrainActions.loadTrainings, (state) => ({...state, loading: true})),
  on (TrainActions.loadTrainingsSuccess, (state, action) => adapter.setAll(action.trainings, {...state, loading: false })),
  on (TrainActions.loadTrainingsFailure, (state, action) => ({...state, error: action.error, loading: false})),
  on (TrainActions.loadTrainingDetails, (state, action) => ({...state, loading: true})),
  on (TrainActions.loadTrainingDetailsSuccess, (state, action) => ({...state, trainingDetails: action.training, loading: false})),
  on (TrainActions.loadTrainingDetailsFailure, (state, action) => ({...state, error: action.error, loading: false})),
  on (TrainActions.addExercise, (state, action) => ({...state})),
  on (TrainActions.createTrainingSuccess, (state, action) => adapter.setOne(action.training, state)),
  on (TrainActions.deleteTrainingSuccess, (state, action) => adapter.removeOne(action.id, state)),
);

export const adapterSelectors = adapter.getSelectors();

 