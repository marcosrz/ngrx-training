import { State, featureKey, adapter } from './train.reducer';
import { createFeatureSelector, createSelector } from "@ngrx/store";


// SELECTORS

// Adapter inherited selectors
const _adapterSelectors = adapter.getSelectors();

// Select feature
export const selectTrain = createFeatureSelector<State>(featureKey);

// select the array of user ids
export const selectTrainingIds = createSelector(selectTrain, _adapterSelectors.selectIds);

// select the dictionary of user entities
export const selectTrainingEntities = createSelector(selectTrain, _adapterSelectors.selectEntities);

// select the array of users
export const selectTrainings = createSelector(selectTrain, _adapterSelectors.selectAll);

// select the total user count
export const count = createSelector(selectTrain, _adapterSelectors.selectTotal);
 
// select the total user count
export const selectLoading = createSelector(selectTrain, (state: State) => state.loading);

// Select error
export const selectError = createSelector(selectTrain, (state: State) => state.error);

// Select One training (trainingDetails)
export const selectTrainingDetails = createSelector(selectTrain, (state: State) => state.trainingDetails);