import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Training } from 'src/app/model/training.model';
import * as fromTrain from '../store/train.reducer';
import * as fromTrainActions from '../store/train.actions';
import * as fromTrainSelectors from '../store/train.selectors';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { Exercise } from 'src/app/model/exercise.model';

@Component({
  selector: 'app-edit-training',
  templateUrl: './edit-training.component.html',
  styleUrls: ['./edit-training.component.scss']
})
export class EditTrainingComponent implements OnInit {

  training!: Training;
  form?: FormGroup;
  editMode = false;

  constructor(
    private store: Store<fromTrain.State>,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    const { paramMap } = this.route.snapshot;
    const id = paramMap.get('id');

    if (id && id.length > 0) {

      this.store.dispatch(fromTrainActions.loadTrainingDetails({ id }))
      
      this.store.select(fromTrainSelectors.selectTrainingDetails).subscribe(training => {
        this.training = {...training};
        this.editMode = true;
        this.initForm();
      });
    } else {
      console.log('Set')
      this.training = new Training();
      this.initForm();
    }
  }
  
  private initForm(){
    let name = '';
    let description = '';
    let date = new Date();

    if (this.training !== undefined){
      name = this.training.name;
      description = this.training.description;
      date = this.training.date;
    }

    this.form = new FormGroup({
      'name': new FormControl(name),
      'description': new FormControl(description),
      'date': new FormControl(date)
    })
  }

  onNewExercise(exercise: Exercise){
    this.training.exercises = [...this.training.exercises, exercise];
  }

  onRemoveExercise(index: number){
    this.training.exercises.splice(index, 1);
  }

  onSave(){
    if (this.training){

      const training = {...this.training, ...(this.form?.value)};

      if (this.editMode){
        this.store.dispatch(fromTrainActions.updateTraining({training}))
      } else {
        this.store.dispatch(fromTrainActions.createTraining({training}))
      }
    }
  }
}

