import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingRoutingModule } from './training-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { TrainingListComponent } from './training-list/training-list.component';
import { EditTrainingComponent } from './edit-training/edit-training.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromTrain from './store/train.reducer';
import {TrainingEffects} from './store/train.effects';


@NgModule({
  declarations: [
    TrainingListComponent,
    EditTrainingComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    TrainingRoutingModule,
    StoreModule.forFeature(fromTrain.featureKey, fromTrain.reducer),
    EffectsModule.forFeature([TrainingEffects]),
  ]
})
export class TrainingModule { }
