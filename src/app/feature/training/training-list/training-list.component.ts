import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Training } from '../../../model/training.model';
import { deleteTraining, loadTrainings } from '../store/train.actions';
import * as fromTrain from '../store/train.reducer'
import * as trainSelectors from '../store/train.selectors'
@Component({
  selector: 'app-training-list',
  templateUrl: './training-list.component.html',
  styleUrls: ['./training-list.component.scss']
})
export class TrainingListComponent implements OnInit {

  trainings$: Observable<Training[]>;
  loading$: Observable<boolean>;
  error$: Observable<string | null>;

  constructor(private store: Store<fromTrain.State>) {

    this.trainings$ = this.store.select(trainSelectors.selectTrainings);
    this.loading$ = this.store.select(trainSelectors.selectLoading);
    this.error$ = this.store.select(trainSelectors.selectError);
  }

  ngOnInit(): void {
    this.store.dispatch(loadTrainings());
  }

  onDelete(id: string){
    this.store.dispatch(deleteTraining({id}))
  }

}
