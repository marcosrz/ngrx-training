import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditTrainingComponent } from './edit-training/edit-training.component';
import { TrainingListComponent } from './training-list/training-list.component';

const routes: Routes = [
  {
    path: '',
    component: TrainingListComponent
  },
  {
    path: 'edit/:id',
    component: EditTrainingComponent
  },
  {
    path: 'edit',
    component: EditTrainingComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingRoutingModule { }
