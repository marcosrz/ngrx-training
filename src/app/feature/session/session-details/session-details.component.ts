import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Session } from 'src/app/model/session.model';

@Component({
  selector: 'app-session-details',
  templateUrl: './session-details.component.html',
  styleUrls: ['./session-details.component.scss']
})
export class SessionDetailsComponent implements OnInit {

  @Input() session?: Session;

  constructor() { }

  ngOnInit(): void {
    
  }
}
