import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateSessionComponent } from './create-session/create-session.component';
import { PlayComponent } from './play/play.component';
import { SessionListComponent } from './session-list/session-list.component';

const routes: Routes = [
  {
    path: '',
    component: SessionListComponent
  },
  {
    path: 'create/:id',
    component: CreateSessionComponent
  },
  {
    path: 'play/:id',
    component: PlayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SessionRoutingModule { }
