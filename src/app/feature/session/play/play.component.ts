import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Location } from '@angular/common'
import { Observable } from 'rxjs';
import { Training } from 'src/app/model/training.model';
import * as trainSelectors from '../../../feature/training/store/train.selectors';
import * as fromTrain from '../../../feature/training/store/train.reducer';
import { loadTrainingDetails } from '../../../feature/training/store/train.actions';
import { ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { Session } from 'src/app/model/session.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {


  constructor(
    // private store: Store<fromTrain.State>,    
    private route: ActivatedRoute,
    private location: Location,
    private sessionService: SessionService
    ) {

    
  }

  ngOnInit(): void {

    const { paramMap } = this.route.snapshot;
    const id = paramMap.get('id');

    if (id && id.length > 0) {
     
    }
  }

  back(){
    this.location.back();
  }

}
