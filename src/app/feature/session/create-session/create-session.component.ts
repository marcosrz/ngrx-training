import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Location } from '@angular/common'
import { Observable } from 'rxjs';
import { Training } from 'src/app/model/training.model';
import * as trainSelectors from '../../../feature/training/store/train.selectors';
import * as fromTrain from '../../../feature/training/store/train.reducer';
import { ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { Session } from 'src/app/model/session.model';
import { Exercise } from 'src/app/model/exercise.model';

@Component({
  selector: 'app-create-session',
  templateUrl: './create-session.component.html',
  styleUrls: ['./create-session.component.scss']
})
export class CreateSessionComponent implements OnInit {

  sessionPrototype$?: Observable<Session>;
  session?: Session;
  // trainingDetails$: Observable<Training>;
  // loading$: Observable<boolean>;
  // error$: Observable<string | null>;

  constructor(
    // private store: Store<fromTrain.State>,    
    private route: ActivatedRoute,
    private location: Location,
    private sessionService: SessionService
    ) {

    // this.trainingDetails$ = this.store.select(trainSelectors.selectTrainingDetails);
    // this.loading$ = this.store.select(trainSelectors.selectLoading);
    // this.error$ = this.store.select(trainSelectors.selectError);
  }

  ngOnInit(): void {

    const { paramMap } = this.route.snapshot;
    const id = paramMap.get('id');

    if (id && id.length > 0) {
      this.sessionPrototype$ = this.sessionService.buildSessionFromTrainingId(id);
      this.sessionPrototype$.subscribe(session => this.session = session);
    }
  }

  addExercise(exercise: Exercise){
    if (this.session){
      this.session.exercises = [...this.session.exercises, exercise];
    }
  }

  onRemoveExercise(index: number){

    if (this.session){
      this.session.exercises.splice(index, 1);
    }
  }

  back(){
    this.location.back();
  }

}
