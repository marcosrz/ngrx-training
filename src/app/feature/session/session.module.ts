import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionListComponent } from './session-list/session-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { SessionRoutingModule } from './session-routing.module';
import { PlayComponent } from './play/play.component';
import { SessionDetailsComponent } from './session-details/session-details.component';
import { CreateSessionComponent } from './create-session/create-session.component';



@NgModule({
  declarations: [
    SessionListComponent,
    PlayComponent,
    SessionDetailsComponent,
    CreateSessionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    SessionRoutingModule
  ]
})
export class SessionModule { }
