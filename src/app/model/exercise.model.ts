import { ExerciseTemplate } from "./exerciseTemplate.model";

export interface Exercise extends ExerciseTemplate{
  reps: number;
  strength: number;
  unit: string;
}