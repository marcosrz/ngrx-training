import { Exercise } from "./exercise.model";

export interface ISession {
  id?: string;
  name: string;
  description: string;
  date: Date;
  exercises: Exercise[];
  trainingId?: string;
  tags: string[]
}
export class Session implements ISession {
  public id?: string;
  public name: string = '';
  public description: string = '';
  public date: Date = new Date();
  public exercises: Exercise[] = [];
  public trainingId?: string  = '';
  public tags: string[] = [];
}