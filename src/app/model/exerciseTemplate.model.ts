export interface ExerciseTemplate {
  id: string;
  name: string;
  series: number;
  description: string;
  tags: string[];
}