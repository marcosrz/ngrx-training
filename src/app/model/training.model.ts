import { Exercise } from "./exercise.model";

export interface ITraining {
  id?: string;
  name: string;
  description: string;
  date: Date;
  exercises: Exercise[];
  tags: string[]
}
export class Training implements ITraining {
  public id!: string;
  public name: string = '';
  public description: string = '';
  public date: Date = new Date();
  public exercises: Exercise[] = [];
  public tags: string[] = [];
}